## On-Boarding

Trainee: `your_username`
Release Manager: Release Manager in your timezone. See https://about.gitlab.com/release-managers/ for details

- [ ] Trainee: Assign yourself and the Release Manager to this issue.
- [ ] Trainer: Assign the new Release Manager [to the correct release manager groups](https://ops.gitlab.net/groups/release-managers/-/group_members)
- [ ] Trainer: Assign the new Release Manager [to the correct infrastructure group](https://ops.gitlab.net/groups/gl-infra/-/group_members)

### Usernames

Trainee: Make a note of your `GitLab.com` and `dev.gitlab.org` usernames and add them to this issue.

|                | Username |
|:---------------|:---------|
| gitlab.com     |          |
| dev.gitlab.org |          |
| ops.gitlab.net |          |

### Access request

- [ ] Trainee: Add your information to the [`config/release_managers.yml`](https://gitlab.com/gitlab-org/release-tools/blob/master/config/release_managers.yml)
  file in release-tools and open a merge request, linking to this issue.
- [ ] Trainee: make sure you can log in to `ops.gitlab.net`. After log in, please change your username to be the same as it is on gitlab.com

### Tool setup

Trainee: Ensure you have completed all the steps on `Access Request` before doing this section.

- [ ] Trainee: Make sure you have [release-tools](https://gitlab.com/gitlab-org/release-tools) cloned locally, and [set it up](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#setup)
- [ ] Trainee: If your ssh key has a passphrase, you will want to do `ssh-add` in your local takeoff repo
- [ ] Trainee: Make sure you have chatops access. If not, ask somebody who does
  to run the following command in Slack: `/chatops run member add USER
  gitlab-com/chatops --ops`, replacing USER with your username on
  ops.gitlab.net.

### First Tasks

- [ ] Trainee: Join #releases on Slack
- [ ] Trainee: Join #incident-management on Slack (incidents may be caused by recent deployment to production)
- [ ] Trainee: Read through the [release guides](https://gitlab.com/gitlab-org/release/docs/blob/master/README.md)
- [ ] Trainee: Read the deploy [docs](https://gitlab.com/gitlab-org/release/docs/tree/master#deployment)
- [ ] Trainee: Be involved in the merge/pick to stable for at least one RC/Patch
- [ ] Trainee: Tag the release for at least one RC/patch
- [ ] Trainee: Deploy to gitlab.com at least once

/label ~"Category:Onboarding"
